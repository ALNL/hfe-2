import "./ShopStyles.css";
import ShopData from "./ShopData";
import Shop1 from "../assets/5.jpg";
import Shop2 from "../assets/8.jpg";
import Shop3 from "../assets/6.jpg";

function Shop() {
  return (
    <div className="trip">
      <h1>Online Shop</h1>
      <p>You can discover our saunas room using this link.</p>
      <div className="tripcard">
        <ShopData
          image={Shop1}
          heading="Walmart"
          text=""
          url="https://www.walmart.com/ip/Hongyuan-Single-Person-EXTENDABLE-Indoor-FAR-Infrared-Heating-Sauna-Bluetooth-Compatible/2134271801"
        />
        <ShopData
          image={Shop2}
          heading="Wayfair"
          text=""
          url="https://www.wayfair.com/outdoor/pdp/royal-saunas-hongyuan-hongyuan-1-person-indoor-bluetooth-compatible-low-emf-far-infrared-in-okoume-hemlock-rshy1017.html"
        />
        <ShopData
          image={Shop3}
          heading="Amazon"
          text=""
          url="https://www.amazon.com/s?i=merchant-items&me=A15Y5R1CE0IPTP"
        />
      </div>
    </div>
  );
}

export default Shop;
